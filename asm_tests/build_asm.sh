file=$1
echo Building file $1 ...
BASENAME=${file//.asm/}
OUTPUTFILE=${BASENAME}.elf

/opt/gcc-mips-7.1.0/bin/mips-elf-as -G 0 -mips1 -o $OUTPUTFILE $file
./elf2mif $OUTPUTFILE .code.mif .data.mif 256 256
echo "v2.0 raw" > code.hex
cat .code.mif >> code.hex

echo "v2.0 raw" > data.hex
cat .data.mif >> data.hex

rm -f .code.mif .data.mif


