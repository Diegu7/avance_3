// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VAlu.h for the primary calling header

#include "VAlu.h"              // For This
#include "VAlu__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VAlu) {
    VAlu__Syms* __restrict vlSymsp = __VlSymsp = new VAlu__Syms(this, name());
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VAlu::__Vconfigure(VAlu__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VAlu::~VAlu() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VAlu::eval() {
    VAlu__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VAlu::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VAlu::_eval_initial_loop(VAlu__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VAlu::_combo__TOP__1(VAlu__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_combo__TOP__1\n"); );
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/Alu.v:9
    vlTOPp->r = ((4U & (IData)(vlTOPp->op)) ? ((2U 
						& (IData)(vlTOPp->op))
					        ? (
						   (1U 
						    & (IData)(vlTOPp->op))
						    ? 
						   VL_LTS_III(32,32,32, vlTOPp->a, vlTOPp->b)
						    : 
						   (~ 
						    (vlTOPp->a 
						     & vlTOPp->b)))
					        : (
						   (1U 
						    & (IData)(vlTOPp->op))
						    ? 
						   (vlTOPp->a 
						    ^ vlTOPp->b)
						    : 
						   (vlTOPp->a 
						    & vlTOPp->b)))
		  : ((2U & (IData)(vlTOPp->op)) ? (
						   (1U 
						    & (IData)(vlTOPp->op))
						    ? 
						   (vlTOPp->a 
						    | vlTOPp->b)
						    : 
						   (vlTOPp->a 
						    * vlTOPp->b))
		      : ((1U & (IData)(vlTOPp->op))
			  ? (vlTOPp->a - vlTOPp->b)
			  : (vlTOPp->a + vlTOPp->b))));
    vlTOPp->z = (0U == vlTOPp->r);
}

void VAlu::_eval(VAlu__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_eval\n"); );
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void VAlu::_eval_initial(VAlu__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_eval_initial\n"); );
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VAlu::final() {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::final\n"); );
    // Variables
    VAlu__Syms* __restrict vlSymsp = this->__VlSymsp;
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VAlu::_eval_settle(VAlu__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_eval_settle\n"); );
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData VAlu::_change_request(VAlu__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_change_request\n"); );
    VAlu* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VAlu::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_ctor_var_reset\n"); );
    // Body
    a = VL_RAND_RESET_I(32);
    b = VL_RAND_RESET_I(32);
    op = VL_RAND_RESET_I(3);
    r = VL_RAND_RESET_I(32);
    z = VL_RAND_RESET_I(1);
}

void VAlu::_configure_coverage(VAlu__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VAlu::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
