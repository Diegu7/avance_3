// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VControlUnit.h for the primary calling header

#include "VControlUnit.h"      // For This
#include "VControlUnit__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VControlUnit) {
    VControlUnit__Syms* __restrict vlSymsp = __VlSymsp = new VControlUnit__Syms(this, name());
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VControlUnit::__Vconfigure(VControlUnit__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VControlUnit::~VControlUnit() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VControlUnit::eval() {
    VControlUnit__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VControlUnit::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VControlUnit::_eval_initial_loop(VControlUnit__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VControlUnit::_combo__TOP__1(VControlUnit__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_combo__TOP__1\n"); );
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/ControlUnit.v:19
    vlTOPp->jmp = 0U;
    vlTOPp->beq = 0U;
    vlTOPp->bne = 0U;
    vlTOPp->memr = 0U;
    vlTOPp->m2r = 0U;
    vlTOPp->memw = 0U;
    vlTOPp->szext = 1U;
    vlTOPp->asrc = 0U;
    vlTOPp->regw = 0U;
    vlTOPp->regd = 0U;
    vlTOPp->aop = 0U;
    if ((0x20U & (IData)(vlTOPp->op))) {
	if ((1U & (~ ((IData)(vlTOPp->op) >> 4U)))) {
	    if ((8U & (IData)(vlTOPp->op))) {
		if ((4U & (IData)(vlTOPp->op))) {
		    if ((1U & (~ ((IData)(vlTOPp->op) 
				  >> 1U)))) {
			if ((1U & (~ (IData)(vlTOPp->op)))) {
			    vlTOPp->regw = 1U;
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->m2r = 0U;
			    vlTOPp->aop = 4U;
			    vlTOPp->szext = 0U;
			}
		    }
		} else {
		    if ((2U & (IData)(vlTOPp->op))) {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->memw = 1U;
			    vlTOPp->aop = 0U;
			}
		    }
		}
	    } else {
		if ((1U & (~ ((IData)(vlTOPp->op) >> 2U)))) {
		    if ((2U & (IData)(vlTOPp->op))) {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->regw = 1U;
			    vlTOPp->memr = 1U;
			    vlTOPp->m2r = 1U;
			    vlTOPp->aop = 0U;
			}
		    }
		}
	    }
	}
    } else {
	if ((1U & (~ ((IData)(vlTOPp->op) >> 4U)))) {
	    if ((8U & (IData)(vlTOPp->op))) {
		if ((4U & (IData)(vlTOPp->op))) {
		    if ((2U & (IData)(vlTOPp->op))) {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->regw = 1U;
			    vlTOPp->asrc = 0U;
			    vlTOPp->regd = 1U;
			    vlTOPp->m2r = 2U;
			}
		    } else {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->regw = 1U;
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->m2r = 0U;
			    vlTOPp->aop = 3U;
			    vlTOPp->szext = 0U;
			}
		    }
		} else {
		    if ((1U & (~ ((IData)(vlTOPp->op) 
				  >> 1U)))) {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->regw = 1U;
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->m2r = 0U;
			    vlTOPp->aop = 0U;
			} else {
			    vlTOPp->regw = 1U;
			    vlTOPp->asrc = 1U;
			    vlTOPp->regd = 0U;
			    vlTOPp->m2r = 0U;
			    vlTOPp->aop = 0U;
			}
		    }
		}
	    } else {
		if ((4U & (IData)(vlTOPp->op))) {
		    if ((1U & (~ ((IData)(vlTOPp->op) 
				  >> 1U)))) {
			if ((1U & (IData)(vlTOPp->op))) {
			    vlTOPp->bne = 1U;
			    vlTOPp->aop = 1U;
			} else {
			    vlTOPp->beq = 1U;
			    vlTOPp->aop = 1U;
			}
		    }
		} else {
		    if ((2U & (IData)(vlTOPp->op))) {
			if ((1U & (~ (IData)(vlTOPp->op)))) {
			    vlTOPp->jmp = 1U;
			}
		    } else {
			if ((1U & (~ (IData)(vlTOPp->op)))) {
			    if ((0x20U & (IData)(vlTOPp->fn))) {
				if ((1U & (~ ((IData)(vlTOPp->fn) 
					      >> 4U)))) {
				    if ((8U & (IData)(vlTOPp->fn))) {
					if ((1U & (~ 
						   ((IData)(vlTOPp->fn) 
						    >> 2U)))) {
					    if ((2U 
						 & (IData)(vlTOPp->fn))) {
						if (
						    (1U 
						     & (~ (IData)(vlTOPp->fn)))) {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 7U;
						}
					    }
					}
				    } else {
					if ((4U & (IData)(vlTOPp->fn))) {
					    if ((1U 
						 & (~ 
						    ((IData)(vlTOPp->fn) 
						     >> 1U)))) {
						if (
						    (1U 
						     & (IData)(vlTOPp->fn))) {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 3U;
						} else {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 4U;
						}
					    }
					} else {
					    if ((2U 
						 & (IData)(vlTOPp->fn))) {
						if (
						    (1U 
						     & (~ (IData)(vlTOPp->fn)))) {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 1U;
						}
					    } else {
						if (
						    (1U 
						     & (IData)(vlTOPp->fn))) {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 0U;
						} else {
						    vlTOPp->regw = 1U;
						    vlTOPp->asrc = 0U;
						    vlTOPp->regd = 1U;
						    vlTOPp->m2r = 0U;
						    vlTOPp->aop = 0U;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
}

void VControlUnit::_eval(VControlUnit__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_eval\n"); );
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void VControlUnit::_eval_initial(VControlUnit__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_eval_initial\n"); );
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VControlUnit::final() {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::final\n"); );
    // Variables
    VControlUnit__Syms* __restrict vlSymsp = this->__VlSymsp;
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VControlUnit::_eval_settle(VControlUnit__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_eval_settle\n"); );
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData VControlUnit::_change_request(VControlUnit__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_change_request\n"); );
    VControlUnit* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VControlUnit::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_ctor_var_reset\n"); );
    // Body
    op = VL_RAND_RESET_I(6);
    fn = VL_RAND_RESET_I(6);
    jmp = VL_RAND_RESET_I(1);
    beq = VL_RAND_RESET_I(1);
    bne = VL_RAND_RESET_I(1);
    memr = VL_RAND_RESET_I(1);
    m2r = VL_RAND_RESET_I(2);
    szext = VL_RAND_RESET_I(1);
    memw = VL_RAND_RESET_I(1);
    asrc = VL_RAND_RESET_I(1);
    regw = VL_RAND_RESET_I(1);
    regd = VL_RAND_RESET_I(1);
    aop = VL_RAND_RESET_I(3);
}

void VControlUnit::_configure_coverage(VControlUnit__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VControlUnit::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
