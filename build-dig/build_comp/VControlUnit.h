// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VControlUnit_H_
#define _VControlUnit_H_

#include "verilated.h"
class VControlUnit__Syms;

//----------

VL_MODULE(VControlUnit) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(op,5,0);
    VL_IN8(fn,5,0);
    VL_OUT8(jmp,0,0);
    VL_OUT8(beq,0,0);
    VL_OUT8(bne,0,0);
    VL_OUT8(memr,0,0);
    VL_OUT8(m2r,1,0);
    VL_OUT8(szext,0,0);
    VL_OUT8(memw,0,0);
    VL_OUT8(asrc,0,0);
    VL_OUT8(regw,0,0);
    VL_OUT8(regd,0,0);
    VL_OUT8(aop,2,0);
    //char	__VpadToAlign13[3];
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign28[4];
    VControlUnit__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VControlUnit& operator= (const VControlUnit&);	///< Copying not allowed
    VControlUnit(const VControlUnit&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VControlUnit(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VControlUnit();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VControlUnit__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VControlUnit__Syms* symsp, bool first);
  private:
    static QData	_change_request(VControlUnit__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__1(VControlUnit__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VControlUnit__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VControlUnit__Syms* __restrict vlSymsp);
    static void	_eval_initial(VControlUnit__Syms* __restrict vlSymsp);
    static void	_eval_settle(VControlUnit__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
