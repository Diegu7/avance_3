// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMemoryDecoder.h for the primary calling header

#include "VMemoryDecoder.h"    // For This
#include "VMemoryDecoder__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMemoryDecoder) {
    VMemoryDecoder__Syms* __restrict vlSymsp = __VlSymsp = new VMemoryDecoder__Syms(this, name());
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VMemoryDecoder::__Vconfigure(VMemoryDecoder__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMemoryDecoder::~VMemoryDecoder() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VMemoryDecoder::eval() {
    VMemoryDecoder__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VMemoryDecoder::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VMemoryDecoder::_eval_initial_loop(VMemoryDecoder__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VMemoryDecoder::_combo__TOP__1(VMemoryDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_combo__TOP__1\n"); );
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/MemoryDecoder.v:9
    if (((0x10010000U <= vlTOPp->in) & (0x10011000U 
					> vlTOPp->in))) {
	vlTOPp->ima = 0U;
	vlTOPp->out = (0x1fffU & vlTOPp->in);
    } else {
	if (((0x7fffeffcU <= vlTOPp->in) & (0x7ffffffcU 
					    > vlTOPp->in))) {
	    vlTOPp->ima = 0U;
	    vlTOPp->out = (0x1fffU & ((IData)(0x1000U) 
				      + (vlTOPp->in 
					 - (IData)(0xffcU))));
	} else {
	    vlTOPp->ima = 1U;
	    vlTOPp->out = 0U;
	}
    }
}

void VMemoryDecoder::_eval(VMemoryDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_eval\n"); );
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void VMemoryDecoder::_eval_initial(VMemoryDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_eval_initial\n"); );
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VMemoryDecoder::final() {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::final\n"); );
    // Variables
    VMemoryDecoder__Syms* __restrict vlSymsp = this->__VlSymsp;
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VMemoryDecoder::_eval_settle(VMemoryDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_eval_settle\n"); );
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData VMemoryDecoder::_change_request(VMemoryDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_change_request\n"); );
    VMemoryDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VMemoryDecoder::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_ctor_var_reset\n"); );
    // Body
    in = VL_RAND_RESET_I(32);
    ima = VL_RAND_RESET_I(1);
    out = VL_RAND_RESET_I(13);
}

void VMemoryDecoder::_configure_coverage(VMemoryDecoder__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VMemoryDecoder::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
