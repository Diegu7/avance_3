// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VPCDecoder.h for the primary calling header

#include "VPCDecoder.h"        // For This
#include "VPCDecoder__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VPCDecoder) {
    VPCDecoder__Syms* __restrict vlSymsp = __VlSymsp = new VPCDecoder__Syms(this, name());
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VPCDecoder::__Vconfigure(VPCDecoder__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VPCDecoder::~VPCDecoder() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VPCDecoder::eval() {
    VPCDecoder__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VPCDecoder::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VPCDecoder::_eval_initial_loop(VPCDecoder__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VPCDecoder::_combo__TOP__1(VPCDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_combo__TOP__1\n"); );
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->out = (0xfffU & vlTOPp->in);
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/PCDecoder.v:9
    vlTOPp->ipc = ((0x400000U > vlTOPp->in) | (0x401000U 
					       <= vlTOPp->in));
}

void VPCDecoder::_eval(VPCDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_eval\n"); );
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void VPCDecoder::_eval_initial(VPCDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_eval_initial\n"); );
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VPCDecoder::final() {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::final\n"); );
    // Variables
    VPCDecoder__Syms* __restrict vlSymsp = this->__VlSymsp;
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VPCDecoder::_eval_settle(VPCDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_eval_settle\n"); );
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData VPCDecoder::_change_request(VPCDecoder__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_change_request\n"); );
    VPCDecoder* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VPCDecoder::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_ctor_var_reset\n"); );
    // Body
    in = VL_RAND_RESET_I(32);
    ipc = VL_RAND_RESET_I(1);
    out = VL_RAND_RESET_I(12);
}

void VPCDecoder::_configure_coverage(VPCDecoder__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VPCDecoder::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
