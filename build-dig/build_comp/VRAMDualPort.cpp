// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VRAMDualPort.h for the primary calling header

#include "VRAMDualPort.h"      // For This
#include "VRAMDualPort__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VRAMDualPort) {
    VRAMDualPort__Syms* __restrict vlSymsp = __VlSymsp = new VRAMDualPort__Syms(this, name());
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VRAMDualPort::__Vconfigure(VRAMDualPort__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VRAMDualPort::~VRAMDualPort() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VRAMDualPort::eval() {
    VRAMDualPort__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VRAMDualPort::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VRAMDualPort::_eval_initial_loop(VRAMDualPort__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VRAMDualPort::_initial__TOP__1(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_initial__TOP__1\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RAMDualPort.v:23
    VL_READMEM_Q (true,32,256, 0,2, VL_ULL(0x646174612e6d6966)
		  , vlTOPp->RAMDualPort__DOT__memory
		  ,0U,0xffU);
}

VL_INLINE_OPT void VRAMDualPort::_sequent__TOP__2(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_sequent__TOP__2\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdlyvdim0__RAMDualPort__DOT__memory__v0,7,0);
    VL_SIG8(__Vdlyvset__RAMDualPort__DOT__memory__v0,0,0);
    //char	__VpadToAlign26[2];
    VL_SIG(__Vdlyvval__RAMDualPort__DOT__memory__v0,31,0);
    // Body
    __Vdlyvset__RAMDualPort__DOT__memory__v0 = 0U;
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RAMDualPort.v:18
    if (vlTOPp->str) {
	__Vdlyvval__RAMDualPort__DOT__memory__v0 = vlTOPp->D_in;
	__Vdlyvset__RAMDualPort__DOT__memory__v0 = 1U;
	__Vdlyvdim0__RAMDualPort__DOT__memory__v0 = vlTOPp->A;
    }
    // ALWAYSPOST at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RAMDualPort.v:20
    if (__Vdlyvset__RAMDualPort__DOT__memory__v0) {
	vlTOPp->RAMDualPort__DOT__memory[__Vdlyvdim0__RAMDualPort__DOT__memory__v0] 
	    = __Vdlyvval__RAMDualPort__DOT__memory__v0;
    }
}

VL_INLINE_OPT void VRAMDualPort::_combo__TOP__3(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_combo__TOP__3\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->RAMDualPort__DOT__D__out__out0 = ((IData)(vlTOPp->ld)
					       ? vlTOPp->RAMDualPort__DOT__memory
					      [vlTOPp->A]
					       : 0U);
}

void VRAMDualPort::_settle__TOP__4(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_settle__TOP__4\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->RAMDualPort__DOT__D__out__out0 = ((IData)(vlTOPp->ld)
					       ? vlTOPp->RAMDualPort__DOT__memory
					      [vlTOPp->A]
					       : 0U);
    vlTOPp->D = ((vlTOPp->RAMDualPort__DOT__D__out__out0 
		  & ((IData)(vlTOPp->ld) ? 0xffffffffU
		      : 0U)) & ((IData)(vlTOPp->ld)
				 ? 0xffffffffU : 0U));
}

VL_INLINE_OPT void VRAMDualPort::_combo__TOP__5(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_combo__TOP__5\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->D = ((vlTOPp->RAMDualPort__DOT__D__out__out0 
		  & ((IData)(vlTOPp->ld) ? 0xffffffffU
		      : 0U)) & ((IData)(vlTOPp->ld)
				 ? 0xffffffffU : 0U));
}

void VRAMDualPort::_eval(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_eval\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->C) & (~ (IData)(vlTOPp->__Vclklast__TOP__C)))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
    }
    vlTOPp->_combo__TOP__3(vlSymsp);
    vlTOPp->_combo__TOP__5(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__C = vlTOPp->C;
}

void VRAMDualPort::_eval_initial(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_eval_initial\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
}

void VRAMDualPort::final() {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::final\n"); );
    // Variables
    VRAMDualPort__Syms* __restrict vlSymsp = this->__VlSymsp;
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VRAMDualPort::_eval_settle(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_eval_settle\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__4(vlSymsp);
}

VL_INLINE_OPT QData VRAMDualPort::_change_request(VRAMDualPort__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_change_request\n"); );
    VRAMDualPort* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VRAMDualPort::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_ctor_var_reset\n"); );
    // Body
    A = VL_RAND_RESET_I(8);
    D_in = VL_RAND_RESET_I(32);
    str = VL_RAND_RESET_I(1);
    C = VL_RAND_RESET_I(1);
    ld = VL_RAND_RESET_I(1);
    D = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<256; ++__Vi0) {
	    RAMDualPort__DOT__memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
    RAMDualPort__DOT__D__out__out0 = VL_RAND_RESET_I(32);
    __Vclklast__TOP__C = VL_RAND_RESET_I(1);
}

void VRAMDualPort::_configure_coverage(VRAMDualPort__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VRAMDualPort::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
