// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VRAMDualPort_H_
#define _VRAMDualPort_H_

#include "verilated.h"
class VRAMDualPort__Syms;

//----------

VL_MODULE(VRAMDualPort) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(C,0,0);
    VL_IN8(A,7,0);
    VL_IN8(str,0,0);
    VL_IN8(ld,0,0);
    VL_IN(D_in,31,0);
    VL_OUT(D,31,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG(RAMDualPort__DOT__memory[256],31,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__C,0,0);
    //char	__VpadToAlign1045[3];
    VL_SIG(RAMDualPort__DOT__D__out__out0,31,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    VRAMDualPort__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VRAMDualPort& operator= (const VRAMDualPort&);	///< Copying not allowed
    VRAMDualPort(const VRAMDualPort&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VRAMDualPort(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VRAMDualPort();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VRAMDualPort__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VRAMDualPort__Syms* symsp, bool first);
  private:
    static QData	_change_request(VRAMDualPort__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__3(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_combo__TOP__5(VRAMDualPort__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VRAMDualPort__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_eval_initial(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_eval_settle(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_initial__TOP__1(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(VRAMDualPort__Syms* __restrict vlSymsp);
    static void	_settle__TOP__4(VRAMDualPort__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
