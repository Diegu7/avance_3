// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VRegFile.h for the primary calling header

#include "VRegFile.h"          // For This
#include "VRegFile__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VRegFile) {
    VRegFile__Syms* __restrict vlSymsp = __VlSymsp = new VRegFile__Syms(this, name());
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VRegFile::__Vconfigure(VRegFile__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VRegFile::~VRegFile() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VRegFile::eval() {
    VRegFile__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VRegFile::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VRegFile::_eval_initial_loop(VRegFile__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VRegFile::_initial__TOP__1(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_initial__TOP__1\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RegFile.v:28
    vlTOPp->RegFile__DOT__registers[9U] = 0x1aU;
    vlTOPp->RegFile__DOT__registers[0xaU] = 0x14U;
}

VL_INLINE_OPT void VRegFile::_sequent__TOP__2(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_sequent__TOP__2\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdlyvdim0__RegFile__DOT__registers__v0,4,0);
    VL_SIG8(__Vdlyvset__RegFile__DOT__registers__v0,0,0);
    //char	__VpadToAlign26[2];
    VL_SIG(__Vdlyvval__RegFile__DOT__registers__v0,31,0);
    // Body
    __Vdlyvset__RegFile__DOT__registers__v0 = 0U;
    // ALWAYS at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RegFile.v:22
    if (vlTOPp->we) {
	__Vdlyvval__RegFile__DOT__registers__v0 = vlTOPp->wd;
	__Vdlyvset__RegFile__DOT__registers__v0 = 1U;
	__Vdlyvdim0__RegFile__DOT__registers__v0 = vlTOPp->wa;
    }
    // ALWAYSPOST at /home/diegu7/trabajos_misc/avance_2/mips32soc_singlecycle/vsrc/RegFile.v:25
    if (__Vdlyvset__RegFile__DOT__registers__v0) {
	vlTOPp->RegFile__DOT__registers[__Vdlyvdim0__RegFile__DOT__registers__v0] 
	    = __Vdlyvval__RegFile__DOT__registers__v0;
    }
}

VL_INLINE_OPT void VRegFile::_combo__TOP__3(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_combo__TOP__3\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->rd1 = vlTOPp->RegFile__DOT__registers[vlTOPp->ra1];
    vlTOPp->rd2 = vlTOPp->RegFile__DOT__registers[vlTOPp->ra2];
}

void VRegFile::_eval(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_eval\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->c) & (~ (IData)(vlTOPp->__Vclklast__TOP__c)))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
    }
    vlTOPp->_combo__TOP__3(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__c = vlTOPp->c;
}

void VRegFile::_eval_initial(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_eval_initial\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
}

void VRegFile::final() {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::final\n"); );
    // Variables
    VRegFile__Syms* __restrict vlSymsp = this->__VlSymsp;
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VRegFile::_eval_settle(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_eval_settle\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__3(vlSymsp);
}

VL_INLINE_OPT QData VRegFile::_change_request(VRegFile__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_change_request\n"); );
    VRegFile* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VRegFile::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_ctor_var_reset\n"); );
    // Body
    ra1 = VL_RAND_RESET_I(5);
    ra2 = VL_RAND_RESET_I(5);
    wa = VL_RAND_RESET_I(5);
    wd = VL_RAND_RESET_I(32);
    we = VL_RAND_RESET_I(1);
    c = VL_RAND_RESET_I(1);
    rd1 = VL_RAND_RESET_I(32);
    rd2 = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<32; ++__Vi0) {
	    RegFile__DOT__registers[__Vi0] = VL_RAND_RESET_I(32);
    }}
    __Vclklast__TOP__c = VL_RAND_RESET_I(1);
}

void VRegFile::_configure_coverage(VRegFile__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VRegFile::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
