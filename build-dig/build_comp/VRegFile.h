// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VRegFile_H_
#define _VRegFile_H_

#include "verilated.h"
class VRegFile__Syms;

//----------

VL_MODULE(VRegFile) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(c,0,0);
    VL_IN8(ra1,4,0);
    VL_IN8(ra2,4,0);
    VL_IN8(wa,4,0);
    VL_IN8(we,0,0);
    //char	__VpadToAlign5[3];
    VL_IN(wd,31,0);
    VL_OUT(rd1,31,0);
    VL_OUT(rd2,31,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG(RegFile__DOT__registers[32],31,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__c,0,0);
    //char	__VpadToAlign157[3];
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign164[4];
    VRegFile__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VRegFile& operator= (const VRegFile&);	///< Copying not allowed
    VRegFile(const VRegFile&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VRegFile(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VRegFile();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VRegFile__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VRegFile__Syms* symsp, bool first);
  private:
    static QData	_change_request(VRegFile__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__3(VRegFile__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VRegFile__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VRegFile__Syms* __restrict vlSymsp);
    static void	_eval_initial(VRegFile__Syms* __restrict vlSymsp);
    static void	_eval_settle(VRegFile__Syms* __restrict vlSymsp);
    static void	_initial__TOP__1(VRegFile__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(VRegFile__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
