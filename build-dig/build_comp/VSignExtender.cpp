// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VSignExtender.h for the primary calling header

#include "VSignExtender.h"     // For This
#include "VSignExtender__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VSignExtender) {
    VSignExtender__Syms* __restrict vlSymsp = __VlSymsp = new VSignExtender__Syms(this, name());
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VSignExtender::__Vconfigure(VSignExtender__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VSignExtender::~VSignExtender() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VSignExtender::eval() {
    VSignExtender__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VSignExtender::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VSignExtender::_eval_initial_loop(VSignExtender__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VSignExtender::_combo__TOP__1(VSignExtender__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_combo__TOP__1\n"); );
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->out = ((0xffff0000U & (((IData)(vlTOPp->szext)
				     ? VL_NEGATE_I((IData)(
							   (1U 
							    & ((IData)(vlTOPp->in) 
							       >> 0xfU))))
				     : 0U) << 0x10U)) 
		   | (IData)(vlTOPp->in));
}

void VSignExtender::_eval(VSignExtender__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_eval\n"); );
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

void VSignExtender::_eval_initial(VSignExtender__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_eval_initial\n"); );
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VSignExtender::final() {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::final\n"); );
    // Variables
    VSignExtender__Syms* __restrict vlSymsp = this->__VlSymsp;
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VSignExtender::_eval_settle(VSignExtender__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_eval_settle\n"); );
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__1(vlSymsp);
}

VL_INLINE_OPT QData VSignExtender::_change_request(VSignExtender__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_change_request\n"); );
    VSignExtender* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

void VSignExtender::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_ctor_var_reset\n"); );
    // Body
    in = VL_RAND_RESET_I(16);
    szext = VL_RAND_RESET_I(1);
    out = VL_RAND_RESET_I(32);
}

void VSignExtender::_configure_coverage(VSignExtender__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VSignExtender::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
