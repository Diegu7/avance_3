// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VSignExtender_H_
#define _VSignExtender_H_

#include "verilated.h"
class VSignExtender__Syms;

//----------

VL_MODULE(VSignExtender) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(szext,0,0);
    //char	__VpadToAlign1[1];
    VL_IN16(in,15,0);
    VL_OUT(out,31,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign20[4];
    VSignExtender__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VSignExtender& operator= (const VSignExtender&);	///< Copying not allowed
    VSignExtender(const VSignExtender&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VSignExtender(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VSignExtender();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VSignExtender__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VSignExtender__Syms* symsp, bool first);
  private:
    static QData	_change_request(VSignExtender__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__1(VSignExtender__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VSignExtender__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VSignExtender__Syms* __restrict vlSymsp);
    static void	_eval_initial(VSignExtender__Syms* __restrict vlSymsp);
    static void	_eval_settle(VSignExtender__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
