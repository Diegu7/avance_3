#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <set>
#include "mips32.h"

using namespace std;

static set<void *> elemInst;

static ElemInfo elemInfo[] = {
    {ALU_ID, SIMPLE_ELEM, -1, "Alu"}, 
    {REGFILE_ID, RAM_ELEM, 32, "RegFile"}, 
    {SIGNEXTENDER_ID, SIMPLE_ELEM, -1, "SignExtender"}, 
    {CONTROLUNIT_ID, SIMPLE_ELEM, -1, "ControlUnit"}, 
    {PCDECODER_ID, SIMPLE_ELEM, -1, "PCDecoder"}, 
    {MEMORYDECODER_ID, SIMPLE_ELEM, -1, "MemoryDecoder"}, 
    {RAMDUALPORT_ID, RAM_ELEM, 256, "RAMDualPort"}, 
};

#define ELEM_COUNT 7
#define MIN_ID 0
#define MAX_ID 6

static int signalCount[] = {
    5, //Alu
    8, //RegFile
    3, //SignExtender
    13, //ControlUnit
    3, //PCDecoder
    3, //MemoryDecoder
    6, //RAMDualPort
};

static ElemSignal alu_signals[] = {
    {INPUT, "a", "", 32},
    {INPUT, "b", "", 32},
    {INPUT, "op", "", 3},
    {OUTPUT, "r", "", 32},
    {OUTPUT, "z", "", 1},
};

static ElemSignal regfile_signals[] = {
    {INPUT, "ra1", "", 5},
    {INPUT, "ra2", "", 5},
    {INPUT, "wa", "", 5},
    {INPUT, "wd", "", 32},
    {INPUT, "we", "", 1},
    {INPUT, "c", "", 1},
    {OUTPUT, "rd1", "", 32},
    {OUTPUT, "rd2", "", 32},
};

static ElemSignal signextender_signals[] = {
    {INPUT, "in", "", 16},
    {INPUT, "szext", "", 1},
    {OUTPUT, "out", "", 32},
};

static ElemSignal controlunit_signals[] = {
    {INPUT, "op", "Opcode", 6},
    {INPUT, "fn", "Function", 6},
    {OUTPUT, "jmp", "Jump signal", 1},
    {OUTPUT, "beq", "BEQ signal", 1},
    {OUTPUT, "bne", "BNE signal", 1},
    {OUTPUT, "memr", "Memory Read", 1},
    {OUTPUT, "m2r", "Register File write data selection", 2},
    {OUTPUT, "szext", "Sign/Zero Extension Selection", 1},
    {OUTPUT, "memw", "Memory Write", 1},
    {OUTPUT, "asrc", "ALU source", 1},
    {OUTPUT, "regw", "Register Write", 1},
    {OUTPUT, "regd", "Register Destination Address selection", 1},
    {OUTPUT, "aop", "ALU operation", 3},
};

static ElemSignal pcdecoder_signals[] = {
    {INPUT, "in", "", 32},
    {OUTPUT, "ipc", "", 1},
    {OUTPUT, "out", "", 12},
};

static ElemSignal memorydecoder_signals[] = {
    {INPUT, "in", "", 32},
    {OUTPUT, "ima", "", 1},
    {OUTPUT, "out", "", 13},
};

static ElemSignal ramdualport_signals[] = {
    {INPUT, "A", "", 8},
    {INPUT, "D_in", "", 32},
    {INPUT, "str", "", 1},
    {INPUT, "C", "", 1},
    {INPUT, "ld", "", 1},
    {OUTPUT, "D", "", 32},
};

static ElemSignal *elemSignals[] = {
    alu_signals, 
    regfile_signals, 
    signextender_signals, 
    controlunit_signals, 
    pcdecoder_signals, 
    memorydecoder_signals, 
    ramdualport_signals, 
};

static void alu_eval(VAlu *elem, uint64_t in[], uint64_t out[]) {
    elem->a = in[0];
    elem->b = in[1];
    elem->op = in[2];
    elem->eval();
    out[0] = elem->r;
    out[1] = elem->z;
}

static void regfile_eval(VRegFile *elem, uint64_t in[], uint64_t out[]) {
    elem->ra1 = in[0];
    elem->ra2 = in[1];
    elem->wa = in[2];
    elem->wd = in[3];
    elem->we = in[4];
    elem->c = in[5];
    elem->eval();
    out[0] = elem->rd1;
    out[1] = elem->rd2;
}

static void signextender_eval(VSignExtender *elem, uint64_t in[], uint64_t out[]) {
    elem->in = in[0];
    elem->szext = in[1];
    elem->eval();
    out[0] = elem->out;
}

static void controlunit_eval(VControlUnit *elem, uint64_t in[], uint64_t out[]) {
    elem->op = in[0];
    elem->fn = in[1];
    elem->eval();
    out[0] = elem->jmp;
    out[1] = elem->beq;
    out[2] = elem->bne;
    out[3] = elem->memr;
    out[4] = elem->m2r;
    out[5] = elem->szext;
    out[6] = elem->memw;
    out[7] = elem->asrc;
    out[8] = elem->regw;
    out[9] = elem->regd;
    out[10] = elem->aop;
}

static void pcdecoder_eval(VPCDecoder *elem, uint64_t in[], uint64_t out[]) {
    elem->in = in[0];
    elem->eval();
    out[0] = elem->ipc;
    out[1] = elem->out;
}

static void memorydecoder_eval(VMemoryDecoder *elem, uint64_t in[], uint64_t out[]) {
    elem->in = in[0];
    elem->eval();
    out[0] = elem->ima;
    out[1] = elem->out;
}

static void ramdualport_eval(VRAMDualPort *elem, uint64_t in[], uint64_t out[]) {
    elem->A = in[0];
    elem->D_in = in[1];
    elem->str = in[2];
    elem->C = in[3];
    elem->ld = in[4];
    elem->eval();
    out[0] = elem->D;
}


/* Exported functions */
EXPORT_C int getElemCount() {
    return ELEM_COUNT;
}

EXPORT_C void getElemInfo(struct ElemInfo outElemInfo[]) {
    for (int i = 0; i < ELEM_COUNT; i++) {
        outElemInfo[i] = elemInfo[i];
    }
}

EXPORT_C int getSignalCount(int elemId) {
    if (elemId >= MIN_ID && elemId <= MAX_ID) {
        return signalCount[elemId];
    }
    return -1;
}

EXPORT_C void *elemCreate(int elemId) {
    void *ptrElem;

    switch (elemId) {
        case ALU_ID:
            ptrElem = new VAlu;
            break;
        case REGFILE_ID:
            ptrElem = new VRegFile;
            break;
        case SIGNEXTENDER_ID:
            ptrElem = new VSignExtender;
            break;
        case CONTROLUNIT_ID:
            ptrElem = new VControlUnit;
            break;
        case PCDECODER_ID:
            ptrElem = new VPCDecoder;
            break;
        case MEMORYDECODER_ID:
            ptrElem = new VMemoryDecoder;
            break;
        case RAMDUALPORT_ID:
            ptrElem = new VRAMDualPort;
            break;
        default:
            return NULL;
    }

    elemInst.insert(ptrElem);
    return ptrElem;
}

EXPORT_C int elemRelease(int elemId, void *ptrElem) {
    if (elemInst.find(ptrElem) != elemInst.end()) {
        elemInst.erase(ptrElem);
        switch (elemId) {
            case ALU_ID:
                delete ((VAlu*)ptrElem);
                break;
            case REGFILE_ID:
                delete ((VRegFile*)ptrElem);
                break;
            case SIGNEXTENDER_ID:
                delete ((VSignExtender*)ptrElem);
                break;
            case CONTROLUNIT_ID:
                delete ((VControlUnit*)ptrElem);
                break;
            case PCDECODER_ID:
                delete ((VPCDecoder*)ptrElem);
                break;
            case MEMORYDECODER_ID:
                delete ((VMemoryDecoder*)ptrElem);
                break;
            case RAMDUALPORT_ID:
                delete ((VRAMDualPort*)ptrElem);
                break;
            default:
                return -1;
        }
        return 0;
    }
    return -1;
}

EXPORT_C int elemEval(int elemId, void *ptrElem, uint64_t in[], uint64_t out[]) {
    if (elemInst.find(ptrElem) != elemInst.end()) {
        switch (elemId) {
            case ALU_ID:
                alu_eval((VAlu*)ptrElem, in, out);
                break;
            case REGFILE_ID:
                regfile_eval((VRegFile*)ptrElem, in, out);
                break;
            case SIGNEXTENDER_ID:
                signextender_eval((VSignExtender*)ptrElem, in, out);
                break;
            case CONTROLUNIT_ID:
                controlunit_eval((VControlUnit*)ptrElem, in, out);
                break;
            case PCDECODER_ID:
                pcdecoder_eval((VPCDecoder*)ptrElem, in, out);
                break;
            case MEMORYDECODER_ID:
                memorydecoder_eval((VMemoryDecoder*)ptrElem, in, out);
                break;
            case RAMDUALPORT_ID:
                ramdualport_eval((VRAMDualPort*)ptrElem, in, out);
                break;
            default:
                return -1;
        }
        return 0;
    } else {
        return -1;
    }
}

EXPORT_C int elemSetData(int elemId, void *ptrElem, int index, uint64_t data) {
    if (elemInst.find(ptrElem) != elemInst.end()) {
        switch (elemId) {
            case REGFILE_ID:
                regfile_setData((VRegFile*)ptrElem, index, data);
                break;
            case RAMDUALPORT_ID:
                ramdualport_setData((VRAMDualPort*)ptrElem, index, data);
                break;
            default:
                return -1;
        }
        return 0;
    } else {
        return -1;
    }
}

EXPORT_C int elemGetData(int elemId, void *ptrElem, uint64_t outData[]) {
    if (elemInst.find(ptrElem) != elemInst.end()) {
        switch (elemId) {
            case REGFILE_ID:
                regfile_getData((VRegFile*)ptrElem, outData);
                break;
            case RAMDUALPORT_ID:
                ramdualport_getData((VRAMDualPort*)ptrElem, outData);
                break;
            default:
                return -1;
        }
        return 0;
    } else {
        return -1;
    }
}

EXPORT_C int getElemSignals(int elemId, struct ElemSignal outElemSignals[]) {
   if (elemId >= MIN_ID && elemId <= MAX_ID) {
       struct ElemSignal *signals = elemSignals[elemId];
       int count = signalCount[elemId];

       for (int i = 0; i < count; i++) {
           outElemSignals[i] = signals[i];
       }
       return 0;
   }
   return -1;
}

