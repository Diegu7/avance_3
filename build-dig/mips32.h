#ifndef __MIPS32_H
#define __MIPS32_H

#include <stdint.h>
#include "VAlu.h"
#include "VRegFile.h"
#include "VSignExtender.h"
#include "VControlUnit.h"
#include "VPCDecoder.h"
#include "VMemoryDecoder.h"
#include "VRAMDualPort.h"

#define INPUT  0
#define OUTPUT 1

#define EXPORT_C extern "C"

#define SIMPLE_ELEM 0
#define RAM_ELEM 1

struct ElemSignal {
    int type;
    const char *name;
    const char *desc;
    int bits;
};

struct ElemInfo {
    uint32_t id;
    uint32_t type;
    uint32_t dataSize;
    const char *name;
};

struct DigElem {
    ElemInfo   *info;
    ElemSignal *signals;
};

#define ALU_ID 0
#define REGFILE_ID 1
#define SIGNEXTENDER_ID 2
#define CONTROLUNIT_ID 3
#define PCDECODER_ID 4
#define MEMORYDECODER_ID 5
#define RAMDUALPORT_ID 6

int regfile_getData(VRegFile *elem, uint64_t outData[]);
int regfile_setData(VRegFile *elem, int index, uint64_t data);
int ramdualport_getData(VRAMDualPort *elem, uint64_t outData[]);
int ramdualport_setData(VRAMDualPort *elem, int index, uint64_t data);

#endif