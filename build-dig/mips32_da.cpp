#include "VAlu.h"
#include "VRegFile.h"
#include "VSignExtender.h"
#include "VControlUnit.h"
#include "mips32.h"

int alu_getData(VAlu *elem, uint64_t outData[]) {
    // TODO: Implement getData
    return -1;
}

int alu_setData(VAlu *elem, int index, uint64_t data) {
    // TODO: Implement setData
    return -1;
}

int alu_getDataSize(VAlu *elem) {
    // TODO: Implement getData
    return 0;
}

int regfile_getData(VRegFile *elem, uint64_t outData[]) {
  for(int x = 0;x<32;x++)
  {
      outData[x] = elem->RegFile__DOT__registers[x];
  }
}

int regfile_setData(VRegFile *elem, int index, uint64_t data) {
    if(index >=0 && index < 32)
    {
      elem->RegFile__DOT__registers[index] = data;
      return 0;
    }
    return -1;
}

int regfile_getDataSize(VRegFile *elem) {
    // TODO: Implement getData
    return 32;
}

int signextender_getData(VSignExtender *elem, uint64_t outData[]) {
    // TODO: Implement getData
    return -1;
}

int signextender_setData(VSignExtender *elem, int index, uint64_t data) {
    // TODO: Implement setData
    return -1;
}

int signextender_getDataSize(VSignExtender *elem) {
    // TODO: Implement getData
    return 0;
}

int controlunit_getData(VControlUnit *elem, uint64_t outData[]) {
    // TODO: Implement getData
    return -1;
}

int controlunit_setData(VControlUnit *elem, int index, uint64_t data) {
    // TODO: Implement setData
    return -1;
}

int controlunit_getDataSize(VControlUnit *elem) {
    // TODO: Implement getData
    return 0;
}
