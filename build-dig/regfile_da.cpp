#include "VRegFile.h"
#include "mips32.h"

int regfile_getData(VRegFile *elem, uint64_t outData[]) {
    for(int x = 0;x<32;x++){
        outData[x] = elem->RegFile__DOT__registers[x];
    }
    return 0;
}

int regfile_setData(VRegFile *elem, int index, uint64_t data) {
    if(index >=0 && index < 32)    {
      elem->RegFile__DOT__registers[index] = data;
      return 0;
    }
    return -1;
}

