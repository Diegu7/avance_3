#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <verilated.h>
#include "Vmips32soc.h"

using namespace std;

void initRegisters(Vmips32soc *m) {
    m->mips32soc__DOT__regFile0__DOT__registers[9] = -1;  
    m->mips32soc__DOT__regFile0__DOT__registers[10] = 20;
}

void dumpDataMemory(Vmips32soc *m) {
    for (int i = 0; i < 16; i++) {
        printf("%04X ", m->mips32soc__DOT__ramdualPort0__DOT__memory[i]);
    }
    cout << dec << endl;
}

void dumpRegs(Vmips32soc *m) {
    for (int i = 0; i < 32; i++) {
        cout << "reg[" << i << "] = " << 
             m->mips32soc__DOT__regFile0__DOT__registers[i] << endl;
    }
}

void dumpSignals(Vmips32soc *m) {
    cout << "inst = " << hex << m->mips32soc__DOT__rom0__DOT__dout__out__out0 << dec << " "
         << "iszero = " << (int)m->mips32soc__DOT__alu0_z << " "
         << "beq = " << (int)m->mips32soc__DOT__beq << " "
         << "regdst = " << (int)m->mips32soc__DOT__regdst << " "
         << "asrc = " << (int)m->mips32soc__DOT__asrc << " "
         << "aluop = " << (int)m->mips32soc__DOT__aluop << " "
         << "memr = " << (int)m->mips32soc__DOT__memr << " "
         << "regw = " << (int)m->mips32soc__DOT__regw << " "
         << "jmp = " << (int)m->mips32soc__DOT__jmp << " "
         << "mw = " << (int)m->mips32soc__DOT__mw << " "
         << "m2r = " << (int)m->mips32soc__DOT__m2r << " "
         << "branch taken = " << (int)m->mips32soc__DOT__or0out << " " << endl;
         //<< "rf_wd = " << m-> << endl;
}

int main(int argc, char** argv)
{
    Vmips32soc *msoc = new Vmips32soc;
    
    msoc->clk = 0;
    msoc->eval();
    
    for (int i = 0; i < 10; i++) {
        msoc->clk = !msoc->clk;
        if (msoc->clk == 1) {
            dumpSignals(msoc);
        }
        msoc->eval();
    }
    dumpDataMemory(msoc);
    dumpRegs(msoc);
    msoc->final();               // Done simulating
    delete msoc;

    return 0;
}
