#!/bin/bash

DMAKE=./tools/dmake
OUTPUT_DIR=build-dig
OUTPUT_LIB=mips32
VSRC=vsrc

# -t option generate a data access template file
$DMAKE -t -D $OUTPUT_DIR -l $OUTPUT_LIB $VSRC/Alu.v \
                                         $VSRC/RegFile.v \
                                         $VSRC/SignExtender.v \
                                         $VSRC/ControlUnit.v \
                                         $VSRC/PCDecoder.v \
                                         $VSRC/MemoryDecoder.v \
                                         $VSRC/RAMDualPort.v
