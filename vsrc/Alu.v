module Alu(
    input [31:0] a,
    input [31:0] b,
    input [2:0] op,
    output reg [31:0] r,
    output reg z
    );

    always @ (op or a or b)
    begin
        case (op)
            3'b000: r = a + b; 
            3'b001: r = a - b;
            3'b010: r = a * b;
            3'b011: r = a | b;
            3'b100: r = a & b;
            3'b101: r = a ^ b;
            3'b110: r = ~(a & b);
            3'b111: r = {31'd0,$signed(a) < $signed(b)};
            default:
                    r = 32'hX;
        endcase
        z = (r == 0);
    end

endmodule
