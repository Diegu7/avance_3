`include "opcodes.vh" 

module ControlUnit(
    input [5:0] op, //! Opcode
    input [5:0] fn, //! Function
    output jmp, //! Jump signal
    output beq, //! BEQ signal
    output bne, //! BNE signal
    output memr, //! Memory Read
    output [1:0] m2r, //! Register File write data selection
    output szext, //! Sign/Zero Extension Selection
    output memw, //! Memory Write
    output asrc, //! ALU source
    output regw, //! Register Write
    output regd, //! Register Destination Address selection
    output [2:0] aop //! ALU operation
);

always @ (op or fn)
begin
    jmp = 1'b0;
    beq = 1'b0;
    bne = 1'b0;
    memr = 1'b0;
    m2r = 2'b0;
    memw = 1'b0;
    szext = 1'b1;
    asrc = 1'b0;
    regw = 1'b0;
    regd = 1'b0;
    aop = 3'b000;

    case (op)
        6'h0:
            case (fn)
            `ADD: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b000;
                end
            `ADDU: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b000;
                end
            `SUB: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b001;
                end
            `AND: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b100;
                end
            `OR: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b011;
                end
            `SLT: 
                begin
                    regw = 1'b1;
                    asrc = 1'b0;
                    regd = 1'b1;
                    m2r = 2'b0;
                    aop = 3'b111;
                end
            default: begin
            end
            endcase
        `ADDI: 
            begin
                regw = 1'b1;
                asrc = 1'b1;
                regd = 1'b0;
                m2r = 2'b0;
                aop = 3'b000;
                end
        `ADDIU: 
            begin
                regw = 1'b1;
                asrc = 1'b1;
                regd = 1'b0;
                m2r = 2'b0;
                aop = 3'b000;
            end
        `ORI: 
            begin
                regw = 1'b1;
                asrc = 1'b1;
                regd = 1'b0;
                m2r = 2'b0;
                aop = 3'b011;
                szext = 1'b0;
            end
        `ANDI: 
            begin
                regw = 1'b1;
                asrc = 1'b1;
                regd = 1'b0;
                m2r = 2'b0;
                aop = 3'b100;
                szext = 1'b0;
            end
        
        `LW:
            begin
                asrc = 1'b1;
                regd = 1'b0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 3'b000;
            end
        `LUI:
            begin
                regw = 1'b1;
                asrc = 1'b0;
                regd = 1'b1;
                m2r = 2'b10;

            end
        `SW:
            begin
                asrc = 1'b1;
                regd = 1'b0;
                memw = 'b1;
                aop = 3'b000;
            end
        `BEQ:
            begin
              beq = 1'b1;
              aop = 3'b001;
            end
        `BNE:
            begin
                bne = 1'b1;
                aop = 3'b001;
            end
        `JUMP:
            begin
              jmp = 1'b1;
            end
        default: begin
        end
    endcase
end

endmodule

