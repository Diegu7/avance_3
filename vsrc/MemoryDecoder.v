module MemoryDecoder (
    input [31:0] in,
    output ima,
    output [12:0] out
);

//assign out = in[11:0];

always@(in)begin
	if(in >= 32'h10010000 && in < 32'h10011000)begin
		ima = 1'b0;
		out = in[12:0];
	end
	else if(in >= 32'h7FFFEFFC && in < 32'h7FFFFFFC)begin
		ima = 1'b0;
		out = in[12:0] - 13'hEFFC + 13'h1000;
	end
	else begin
		ima = 1'b1;
		out = 13'hX;
	end
end


endmodule

