module PCDecoder (
    input [31:0] in,
    output ipc,
    output [11:0] out
);

assign out = in[11:0];

always@(in)begin
	if(in < 32'h400000 || in >= 32'h401000) ipc = 1'b1;
	else ipc = 1'b0;
end

    //assign out = {szext ? {16{in[15]}} : {16{1'b0}}, in};

endmodule

