/* verilator lint_off WIDTH */
/* verilator lint_off UNUSED */
module mips32soc(
input clk
);

/* Declarations (registers and wires) */
wire [31:0] alu0_r;
wire alu0_z;
wire [31:0] regFile0_rd2;
wire [31:0] regFile0_rd1;
wire beq;
wire regdst;
wire asrc;
wire [2:0] aluop;
wire memr;
wire regw;
wire jmp;
wire mw;
wire m2r;
wire and0out;
wire [31:0] signExtender0_out;
wire [31:0] pc_4; 
reg [31:0] pc_in;
reg [31:0] pc;
wire [31:0] inst;
reg [4:0] mux0out;
wire [31:0] ramdualPort0_D;
reg [31:0] mux1out;
reg [31:0] mux2out;

/* Assign expressions */
assign pc_4 = pc + 32'h4;
assign and0out = beq & alu0_z;

/* Always blocks */
always @ (posedge clk) begin
  pc <= pc_in;
end
always @ (*) begin
  case (jmp)
    1'h0: begin
      case (and0out)
        1'h0: pc_in = pc + 32'h4;
        1'h1: pc_in = pc + 32'h4 + {signExtender0_out[29:0], 2'h0};
        default: pc_in = 32'hx;
      endcase
    end
    1'h1: pc_in = {pc_4[31:28], {inst[25:0], 2'h0}};
    default: pc_in = 32'hx;
  endcase
end
always @ (*) begin
  case (m2r)
    1'h0: mux1out = alu0_r;
    1'h1: mux1out = ramdualPort0_D;
    default: mux1out = 32'hx;
  endcase
end
always @ (*) begin
  case (asrc)
    1'h0: mux2out = regFile0_rd2;
    1'h1: mux2out = signExtender0_out;
    default: mux2out = 32'hx;
  endcase
end
always @ (*) begin
  case (regdst)
    1'h0: mux0out = inst[20:16];
    1'h1: mux0out = inst[15:11];
    default: mux0out = 5'hx;
  endcase
end

/* Instances and always blocks */
/* ControlUnit
 */
ControlUnit controlUnit0(
  .op(inst[31:26]),
  .fn(inst[5:0]),
  .b(beq),
  .rd(regdst),
  .as(asrc),
  .aop(aluop),
  .mr(memr),
  .rw(regw),
  .j(jmp),
  .mw(mw),
  .mtr(m2r)
);
/* SignExtender
 */
SignExtender signExtender0(
  .in(inst[15:0]),
  .out(signExtender0_out)
);
// Asynchronous ROM (Data Bits = 32, Address Bits = 8)
// Init file = code.hex
AsyncROM #(.Bits(32), .AddrBits(8)) rom0 (
  .addr(pc[9:2]),
  .en(1'h1),
  .dout(inst)
);
/* RAMDualPort
 * Bits=32
 * Addr Bits=8
 * lastDataFile=/home/ideras/classes/Computer_Organization/Project-MIPS32SOC/P12018/asm_test1/data.hex
 * autoReload=true
 */
RAMDualPort ramdualPort0(
  .str(mw),
  .D_in(regFile0_rd2),
  .A(alu0_r[9:2]),
  .C(clk),
  .ld(memr),
  .D(ramdualPort0_D)
);
/* RegFile
 */
RegFile regFile0(
  .ra2(inst[20:16]),
  .c(clk),
  .ra1(inst[25:21]),
  .wa(mux0out),
  .wd(mux1out),
  .we(regw),
  .rd2(regFile0_rd2),
  .rd1(regFile0_rd1)
);
/* Alu
 */
Alu alu0(
  .a(regFile0_rd1),
  .op(aluop),
  .b(mux2out),
  .r(alu0_r),
  .z(alu0_z)
);

endmodule
